import 'dart:convert';
import '../models/users.dart';
import 'package:http/http.dart' as http;


class RemoteService {
  Future<Users?> getUsers() async {
    var client = http.Client();
    var uri = Uri.parse(
        'https://reqres.in/api/users?page=2');
    var response = await client.get(uri);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      return Users.fromJson(data);

    } else {
      return null;
    }
  }
}