import 'package:flutter/material.dart';
import '../models/users.dart';
import '../services/remote_service.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Users? users;
  bool isLoaded = false;
  getUserData()async{
    users = await RemoteService().getUsers();
    if(users != null){
      setState(() {
        isLoaded = true;

      });
    }
  }


  @override
  void initState() {
  getUserData();
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return  SafeArea(
      child: Scaffold(body:
      Visibility(
        visible: isLoaded,
        replacement: const Center(child: CircularProgressIndicator()),
        child: ListView.builder(
          itemCount: users== null ? 0 :users!.data!.length,
          itemBuilder: (context,index){
          return  ListTile(
            title: Text(users!.data![index]!.email!),
          );
        },
        ),
      )
      ),
    );
  }
}
